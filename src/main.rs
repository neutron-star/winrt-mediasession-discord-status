use std::{env, thread, time};
use winrt::*;
include_bindings!();
use windows::media::control::*;
use windows::media::MediaPlaybackType;
use discord_rpc_client::Client;

const CLIENT_ID: u64 = 787138231577477120;

// @TODO use MediaPropertiesChanged/PlaybackInfoChanged events

fn get_media_session_manager() -> Result<GlobalSystemMediaTransportControlsSessionManager> {
    return GlobalSystemMediaTransportControlsSessionManager::request_async()?.get()
}

fn get_media_session(manager: &GlobalSystemMediaTransportControlsSessionManager) -> Result<GlobalSystemMediaTransportControlsSession> {
    return manager.get_current_session();
}

fn get_media_properties(session: &GlobalSystemMediaTransportControlsSession) -> Result<GlobalSystemMediaTransportControlsSessionMediaProperties> {
    return session.try_get_media_properties_async()?.get();
}

fn get_playback_status(session: &GlobalSystemMediaTransportControlsSession) -> Result<GlobalSystemMediaTransportControlsSessionPlaybackStatus> {
    return session.get_playback_info()?.playback_status();
}

fn get_and_set(drpc: &mut Client, manager: &GlobalSystemMediaTransportControlsSessionManager) -> std::result::Result<(), String> {
    let session = match get_media_session(&manager) {
        Ok(session) => session,
        Err(err) => return Err(format!("Failed to get current media session: {}", err.message()))
    };
    let media_properties = match get_media_properties(&session) {
        Ok(media_properties) => media_properties,
        Err(err) => return Err(format!("Failed to get current media properties: {}", err.message()))
    };
    let title = match media_properties.title() {
        Ok(title) => title,
        Err(err) => return Err(format!("Failed to retrieve media title: {}", err.message()))
    };
    let playback_type = match media_properties.playback_type() {
        Ok(playback_type) => match playback_type.value() {
            Ok(playback_type_val) => playback_type_val,
            Err(err) => return Err(format!("Failed to retrieve media playback type value: {}", err.message()))
        },
        Err(err) => return Err(format!("Failed to retrieve media playback type: {}", err.message()))
    };

    if playback_type != MediaPlaybackType::Music {
        match drpc.clear_activity() {
            Err(why) => println!("Failed clearing activity: {}", why),
            Ok(_) => ()
        };
        return Ok(())
    }

    match drpc.set_activity(
        |mut act| {
            println!("Title: {}", title.to_string());
            match media_properties.artist() {
                Ok(artist) => {
                    println!("Artist: {}", artist.to_string());
                    act = act.state(format!("by {}", artist.to_string()));
                },
                Err(err) => println!("Failed to retrieve artist: {}", err.message())
            }
            return act
            .details(title.to_string())
            .assets(|mut ass| {
                ass = ass
                .large_image("tidal-logo")
                .large_text("TIDAL");
                match get_playback_status(&session) {
                    Ok(playback_status) =>
                        {
                            if playback_status == GlobalSystemMediaTransportControlsSessionPlaybackStatus::Playing {
                                println!("Currently playing");
                                ass = ass.small_image("play").small_text("Playing")
                            } else {
                                println!("Paused");
                                ass = ass.small_image("pause").small_text("Paused")
                            }
                        },
                    Err(err) => println!("Failed getting playback status: {}", err.message())
                }
                return ass
            })
        }
    ) {
        Ok(_) => (),
        Err(err) => println!("Failed to set activity: {}", err)
    };
    Ok(())
}


fn main() -> Result<()> {
    let manager = match get_media_session_manager() {
        Ok(manager) => manager,
        Err(err) => panic!("Failed to get media session manager (this app requires at least Windows 10 1809): {}", err.message())
    };
    match Client::new(CLIENT_ID) {
        Ok(mut drpc) => {
            drpc.start();
            println!("Created and started Discord RPC client");
            loop {
                if let Err(why) = get_and_set(&mut drpc, &manager) {
                    println!("Failed setting activity: {}, clearing activity", why);
                    if let Err(why) = drpc.clear_activity() {
                        println!("Failed clearing activity: {}", why);
                    };
                };
                thread::sleep(time::Duration::from_secs(3));
            }
        },
        Err(err) => panic!("Failed to create Discord RPC client: {}", err)
    };
}
